# Readme
This is a Demo implementation of WordPress Nonces. WordPress Nonces are
objects which are unique and have a limited lifetime. They are used to validate
if an action on a WordPress page is allowed to prevent CSRF attacks.

## Idea
The interface "Nonce" represents a Nonce and describes the functions that
can be done on it. It is created through its constructor and can be injected
into an application using Dependency Injection. The wp_create_nonce function
is no longer required because everytime you do an action on "Nonce" you already
have an instance of it.
The class "Url" is just a wrapper for a string and is used to show to the
implementor of interfaces that an url is required as a parameter. If necessary,
the class could later be improved that way, that the single parts of an URL are
passed as constructor parameters and then are combined to a correctly formed
URL string by "format_url".

## Implementation
The class "CountingDateTimeNonce" is a reference implementation of a Nonce.
It has an internal integer-based counter which is incremented by one each time
a Nonce is created. The constructor calculates the hash sum by concatenating
the current number and the current date and time. This way, it is ensured, that
the Nonce is unique even if the integer flows over or if multiple Nonces are
created in a split of time between 2 values of the DateTime.
To make the Nonce implementations testable by Unit tests, I decided to add
the parameter "$referer" to the "wp_nonce_field" function. Also I removed argument
"echo" form the function, to improve testability and code quality, so the function
now always returns a string which then can be passed to an "echo" statement by
the caller.