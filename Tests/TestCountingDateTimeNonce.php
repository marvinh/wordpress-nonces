<?php namespace Demo;

/* 
 * Unit test for "CountingDateTimeNonce"
 */

use PHPUnit\Framework\TestCase;
use DateInterval;
use Demo\Classes\CountingDateTimeNonce;
use Spatie\Url\Url;

class TestCountingDateTimeNonce extends TestCase{
    public function testWpNonceField_WithoutReferer(){
        $nonce = new CountingDateTimeNonce();
        $hash = $nonce->get_hash();
        self::assertTrue($nonce->wp_nonce_field() == '<input type="hidden" id="save_cpt_wpnonce" value="'.$hash.'">');
    }
    
    public function testWpNonceField_WithReferer(){
        $nonce = new CountingDateTimeNonce();
        $hash = $nonce->get_hash();
        self::assertTrue($nonce->wp_nonce_field("http://test.de") == '<input type="hidden" id="save_cpt_wpnonce" value="'.$hash.'">'
                . '<input type="hidden" name="_wp_http_referer" value="http://test.de">');
    }
    
    public function testWpNonceUrl_WithoutParams(){
        $nonce = new CountingDateTimeNonce();
        $hash = $nonce->get_hash();
        $nonced_url = $nonce->wp_nonce_url(Url::fromString("http://test.de"));
        self::assertTrue($nonced_url == 'http://test.de?_wpnonce='.$hash.'', 
                "Returned URL: ".$nonced_url."\n".'Expected URL: http://test.de?_wpnonce='.$hash.'');
    }
    
    public function testWpNonceUrl_WithAction(){
        $nonce = new CountingDateTimeNonce("trash");
        $hash = $nonce->get_hash();
        $nonced_url = $nonce->wp_nonce_url(Url::fromString("http://test.de"));
        self::assertTrue($nonced_url == 'http://test.de?action=trash&_wpnonce='.$hash.'', 
                "Returned URL: ".$nonced_url."\n".'Expected URL: http://test.de?action=trash&_wpnonce='.$hash.'');
    }
    
    public function testWpNonceUrl_WithName(){
        $nonce = new CountingDateTimeNonce("trash", "testNonce");
        $hash = $nonce->get_hash();
        $nonced_url = $nonce->wp_nonce_url(Url::fromString("http://test.de"));
        self::assertTrue($nonced_url == 'http://test.de?action=trash&testNonce='.$hash.'', 
                "Returned URL: ".$nonced_url."\n".'Expected URL: http://test.de?action=trash&testNonce='.$hash.'');
    }
    
    public function testWpVerifyNonce_NonceValid(){
        $validNonce = new CountingDateTimeNonce();
        self::assertTrue($validNonce->wp_verify_nonce());
    }
    
    public function testWpVerifyNonce_NonceInvalid(){
        $inValidNonce = new CountingDateTimeNonce("", "_wpnonce", new DateInterval("PT1S"));
        sleep(2);
        self::assertFalse($inValidNonce->wp_verify_nonce());
    }
}